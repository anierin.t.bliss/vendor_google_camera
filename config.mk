RELAX_USES_LIBRARY_CHECK := true

PRODUCT_PACKAGES += \
    LMCGoogleCamera

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/system_ext/etc/permissions/com.google.android.GoogleCameraEng.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/com.google.android.GoogleCameraEng.xml
